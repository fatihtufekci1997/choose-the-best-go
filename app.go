package main

import (
	"log"
	"os"
	"quiz-be/routes"

	"quiz-be/database"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/joho/godotenv"
)

func setUpRoutes(app *fiber.App) {
	// Static routes
	app.Static("/s/", "./media")

	// Category routes
	app.Get("/categories", routes.CategoryList)
	app.Get("/categories/:id", routes.CategoryDetail)
	app.Post("/categories", routes.CategoryCreate)

	// Attachment routes
	app.Get("/attachments", routes.AttachmentList)
	app.Get("/attachments/:id", routes.AttachmentDetail)
	app.Post("/attachments", routes.AttachmentCreate)
	app.Put("/attachments/:id/set-score", routes.AttachmentSetScore)
	app.Patch("attachments/update-titles", routes.AttachmentUpdateTitles)

	// Quiz routes
	app.Get("/quizes", routes.QuizList)
	app.Get("/quizes/top-rated", routes.QuizTopRatedList)
	app.Get("/quizes/top-rated-avg", routes.QuizTopRatedWithAvgList)
	app.Post("/quizes", routes.QuizCreate)
	app.Get("/quizes/:id", routes.QuizDetail)

	// QuizRate routes
	app.Get("/quiz-rates", routes.QuizRateList)
	app.Get("/quiz-rates/:id", routes.QuizRateDetail)
	app.Post("/quiz-rates", routes.QuizRateCreate)
}

func main() {
	err := godotenv.Load("envs/.env")
	if err != nil {
		log.Fatal(err)
	}
	config := &database.Config{
		Host:     os.Getenv("POSTGRES_HOST"),
		Port:     os.Getenv("POSTGRES_PORT"),
		Password: os.Getenv("POSTGRES_PASSWORD"),
		User:     os.Getenv("POSTGRES_USER"),
		SSLMode:  os.Getenv("POSTGRES_SSLMODE"),
		DBName:   os.Getenv("POSTGRES_DB"),
	}

	database.ConnectDb(config)
	app := fiber.New()

	setUpRoutes(app)

	app.Use(cors.New())

	app.Use(func(c *fiber.Ctx) error {
		return c.SendStatus(404) // => 404 "Not Found"
	})

	log.Fatal(app.Listen(":8000"))
}
