package database

import (
	"fmt"
	"log"
	"quiz-be/models"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type Dbinstance struct {
	Db *gorm.DB
}

var DB Dbinstance

type Config struct {
	Host     string
	Port     string
	Password string
	User     string
	DBName   string
	SSLMode  string
}

// connectDb
func ConnectDb(config *Config) {
	dsn := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		config.Host, config.Port, config.User, config.Password, config.DBName, config.SSLMode,
	)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	log.Println("connected")
	db.Logger = logger.Default.LogMode(logger.Info)
	log.Println("running migrations")

	// Migrate Models
	db.AutoMigrate(&models.Category{})
	db.AutoMigrate(&models.Quiz{})
	db.AutoMigrate(&models.QuizRate{})
	db.AutoMigrate(&models.Attachment{})

	DB = Dbinstance{
		Db: db,
	}
}
