package schemas

type CategoryCreateSchema struct {
	Name string `json:"name" validate:"required"`
}

type CategoryResponseSchema struct {
	Name string `json:"name"`
}
