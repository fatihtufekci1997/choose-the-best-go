#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

readonly cmd="$*"

echo "APP_ENV is $APP_ENV"

if [ "$APP_ENV" = 'production' ];
then
  go run app.go
else
  GOFLAGS="-buildvcs=false" air -c .air.toml
fi;

exec $cmd
