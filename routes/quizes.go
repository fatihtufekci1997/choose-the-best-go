package routes

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"quiz-be/database"
	"quiz-be/models"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

// QuizList is category list endpoint
func QuizList(c *fiber.Ctx) error {
	var quizes []models.Quiz
	database.DB.Db.
		Preload("Attachments").
		Preload("QuizRates").
		Joins("Category").
		Where("is_visible = ?", true).
		Find(&quizes)
	return c.JSON(quizes)
}

// QuizDetail is category detail endpoint
func QuizDetail(c *fiber.Ctx) error {
	id := c.Params("id")
	var quiz models.Quiz
	database.DB.Db.Preload("Attachments").Joins("Category").First(&quiz, id)
	return c.JSON(quiz)
}

// QuizTopRatedList endpoint
func QuizTopRatedList(c *fiber.Ctx) error {
	var quizes []models.Quiz
	database.DB.Db.
		Preload("QuizRates").
		Preload("Attachments").
		Preload("Category").
		Joins("LEFT JOIN quiz_rates ON quizzes.id = quiz_rates.quiz_id").
		Group("quizzes.id").
		Order("AVG(quiz_rates.rate_score) DESC").
		Find(&quizes)

	return c.JSON(quizes)
}

// QuizTopRatedList endpoint
func QuizTopRatedWithAvgList(c *fiber.Ctx) error {
	var quizes []models.Quiz

	database.DB.Db.Model(&models.Quiz{}).
		Joins("LEFT JOIN quiz_rates ON quizzes.id = quiz_rates.quiz_id").
		Group("quizzes.id").
		Order("AVG(quiz_rates.rate_score) DESC").
		Select("quizzes.*, AVG(quiz_rates.rate_score) as avg_score").
		Find(&quizes)

	return c.JSON(quizes)
}

// QuizCreate is category create endpoint
func QuizCreate(c *fiber.Ctx) error {
	title := c.FormValue("Title")
	isVisible := c.FormValue("IsVisible")
	isImage := c.FormValue("IsImage")
	categoryID := c.FormValue("CategoryID")

	var attachmentIds []uint
	err := json.Unmarshal([]byte(c.FormValue("AttachmentIds")), &attachmentIds)
	if err != nil {
		return c.Status(400).JSON(fiber.Map{"error": "Invalid AttachmentIds"})
	}

	// Parse form data including file upload
	form, err := c.MultipartForm()
	if err != nil {
		return c.Status(400).JSON(fiber.Map{"error": "Invalid request payload"})
	}

	// Get the uploaded file
	formFile := form.File["Image"][0]
	ext := filepath.Ext(formFile.Filename)
	file, err := formFile.Open()
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"error": "Failed to open uploaded file"})
	}
	defer file.Close()

	// Generate a unique filename for the uploaded file
	filename := title
	uniqueFileName := fmt.Sprintf("%s_%s", filename, uuid.New().String())
	uploadPath := fmt.Sprintf("./media/background_images/%s%s", uniqueFileName, ext)

	// Convert categoryID to uint
	var categoryIDUint uint
	if _, err := fmt.Sscan(categoryID, &categoryIDUint); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"error": "Invalid categoryID"})
	}

	// Convert isVisible to bool
	isVisibleBool, err := strconv.ParseBool(isVisible)
	if err != nil {
		log.Fatal(err)
	}

	// Convert isImage to bool
	isImageBool, err := strconv.ParseBool(isImage)
	if err != nil {
		log.Fatal(err)
	}

	// Save the uploaded file to local storage
	dst, err := os.Create(uploadPath)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"error": "Failed to create file on server"})
	}
	defer dst.Close()

	_, err = io.Copy(dst, file)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"error": "Failed to save file on server"})
	}

	// Create a new attachment in the database
	quiz := models.Quiz{
		Title:      title,
		Image:      &uploadPath,
		CategoryID: &categoryIDUint,
		IsVisible:  &isVisibleBool,
		IsImage:    &isImageBool,
	}

	database.DB.Db.Transaction(func(tx *gorm.DB) error {

		if err := tx.Create(&quiz).Error; err != nil {
			// return any error will rollback
			return err
		}

		// Set Attachments quiz value
		var attachments []models.Attachment
		database.DB.Db.Where("id IN ?", attachmentIds).Find(&attachments)
		if len(attachments) != len(attachmentIds) {
			return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"AttachmentIds": "Invalid Attachment Id exists."})
		}

		// Values to update
		updateValues := map[string]interface{}{
			"QuizID": quiz.ID,
		}

		if err := tx.Model(&models.Attachment{}).Where("id IN ?", attachmentIds).Updates(updateValues).Error; err != nil {
			// return any error will rollback
			return err
		}

		// return nil will commit the whole transaction
		return nil

	})

	// Return the created attachment
	return c.Status(201).JSON(quiz)
}
