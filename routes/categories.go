package routes

import (
	"quiz-be/database"
	"quiz-be/models"
	"quiz-be/schemas"
	"quiz-be/validators"

	"github.com/gofiber/fiber/v2"
)

// CategoryList is category list endpoint
func CategoryList(c *fiber.Ctx) error {
	var categories []models.Category
	database.DB.Db.Find(&categories)
	return c.JSON(categories)
}

// CategoryDetail is category detail endpoint
func CategoryDetail(c *fiber.Ctx) error {
	id := c.Params("id")
	var category models.Category
	database.DB.Db.First(&category, id)
	return c.JSON(category)
}

// CategoryCreate is category create endpoint
func CategoryCreate(c *fiber.Ctx) error {
	var schema schemas.CategoryCreateSchema
	if err := c.BodyParser(&schema); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"status": "fail", "message": err.Error()})
	}

	errors := validators.ValidateStruct(schema)
	if errors != nil {
		return c.Status(fiber.StatusBadRequest).JSON(errors)

	}

	category := models.Category{
		Name: schema.Name,
	}
	database.DB.Db.Create(&category)
	return c.JSON(schemas.CategoryResponseSchema{
		Name: category.Name,
	})
}
