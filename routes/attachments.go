package routes

import (
	"path/filepath"
	"quiz-be/database"
	"quiz-be/models"

	"github.com/gofiber/fiber/v2"

	"fmt"
	"io"
	"os"

	"github.com/google/uuid"
)

// AttachmentList is category list endpoint
func AttachmentList(c *fiber.Ctx) error {
	var attachments []models.Attachment
	database.DB.Db.Joins("Quiz").Find(&attachments)
	return c.JSON(attachments)
}

// AttachmentDetail is category detail endpoint
func AttachmentDetail(c *fiber.Ctx) error {
	id := c.Params("id")
	var attachment models.Attachment
	database.DB.Db.Joins("Quiz").First(&attachment, id)
	return c.JSON(attachment)
}

// AttachmentCreate is category create endpoint
func AttachmentCreate(c *fiber.Ctx) error {
	// Parse form data including file upload
	form, err := c.MultipartForm()
	if err != nil {
		return c.Status(400).JSON(fiber.Map{"error": "Invalid request payload"})
	}

	// Get the uploaded file
	formFile := form.File["File"][0]
	ext := filepath.Ext(formFile.Filename)
	file, err := formFile.Open()
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"error": "Failed to open uploaded file"})
	}
	defer file.Close()

	// Generate a unique filename for the uploaded file
	filename := form.Value["Title"][0]
	uniqueFileName := fmt.Sprintf("%s_%s", filename, uuid.New().String())
	uploadPath := fmt.Sprintf("./media/images/%s%s", uniqueFileName, ext)

	// Save the uploaded file to local storage
	dst, err := os.Create(uploadPath)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"error": "Failed to create file on server"})
	}
	defer dst.Close()

	_, err = io.Copy(dst, file)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"error": "Failed to save file on server"})
	}

	// Create a new attachment in the database
	attachment := models.Attachment{
		Title: form.Value["Title"][0],
		Image: &uploadPath,
		Url:   nil,
		Score: 0,
	}

	result := database.DB.Db.Create(&attachment)
	if result.Error != nil {
		return c.Status(500).JSON(fiber.Map{"error": "Failed to create attachment"})
	}

	// Return the created attachment
	return c.Status(201).JSON(attachment)
}

// AttachmentSetScore endpoint
func AttachmentSetScore(c *fiber.Ctx) error {
	id := c.Params("id")
	// Lock the table before updating
	tx := database.DB.Db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	// Find the attachment by ID and lock the row
	var attachment models.Attachment
	if tx.First(&attachment, id).RowsAffected == 0 {
		tx.Rollback()
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"error": "Attachment not found"})
	}

	// Increment the score
	attachment.Score++
	if err := tx.Save(&attachment).Error; err != nil {
		tx.Rollback()
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": "Failed to update score"})
	}

	// Commit the transaction
	tx.Commit()

	// Respond with the updated attachment
	return c.JSON(attachment)
}

type AttachmentTitle struct {
	Ids    []int
	Titles []string
}

// AttachmentUpdateTitles endpoint
func AttachmentUpdateTitles(c *fiber.Ctx) error {
	var attachmentTitle AttachmentTitle
	if err := c.BodyParser(&attachmentTitle); err != nil {
		return c.Status(400).SendString("Invalid JSON format")
	}

	if len(attachmentTitle.Ids) != len(attachmentTitle.Titles) {
		return c.Status(400).SendString("Invalid Request")
	}

	var attachments []models.Attachment
	database.DB.Db.Where("id In ?", attachmentTitle.Ids).Find(&attachments)

	if len(attachments) != len(attachmentTitle.Ids) {
		return c.Status(400).SendString("Invalid attachment id")
	}

	idTitleMap := make(map[int]string)

	for i := 0; i < len(attachmentTitle.Ids); i++ {
		idTitleMap[attachmentTitle.Ids[i]] = attachmentTitle.Titles[i]
	}

	for i := 0; i < len(attachments); i++ {
		attachments[i].Title = idTitleMap[int(attachments[i].ID)]
	}

	database.DB.Db.Save(&attachments)
	return c.SendStatus(200)
}
