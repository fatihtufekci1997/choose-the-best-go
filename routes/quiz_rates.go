package routes

import (
	"quiz-be/database"
	"quiz-be/models"

	"github.com/gofiber/fiber/v2"
)

// QuizRateList is quiz rate list endpoint
func QuizRateList(c *fiber.Ctx) error {
	var quiz_rates []models.QuizRate
	database.DB.Db.Joins("Quiz").Find(&quiz_rates)
	return c.JSON(quiz_rates)
}

// QuizRateDetail is quiz rate detail endpoint
func QuizRateDetail(c *fiber.Ctx) error {
	id := c.Params("id")
	var quiz_rate models.QuizRate
	database.DB.Db.Joins("Quiz").First(&quiz_rate, id)
	return c.JSON(quiz_rate)
}

// QuizRateCreate is quiz rate create endpoint
func QuizRateCreate(c *fiber.Ctx) error {
	var quiz_rate models.QuizRate
	if err := c.BodyParser(&quiz_rate); err != nil {
		return c.Status(400).SendString("Invalid JSON format")
	}
	database.DB.Db.Create(&quiz_rate)
	return c.JSON(quiz_rate)
}
