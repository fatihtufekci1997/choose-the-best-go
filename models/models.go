package models

import (
	"gorm.io/gorm"
)

// Category model
type Category struct {
	gorm.Model

	Name string `json:"Name"`
}

// Quiz model
type Quiz struct {
	gorm.Model

	Title      string    `json:"Title"`
	CategoryID *uint     `json:"CategoryID"`
	Category   *Category `json:"Category"`
	Image      *string   `json:"Image,omitempty" gorm:"default:null"`
	IsVisible  *bool     `json:"IsVisible" gorm:"default:true"`
	IsImage    *bool     `json:"IsImage" gorm:"default:true"`

	Attachments []Attachment `json:"Attachments"`
	QuizRates   []QuizRate   `json:"QuizRates"`

	AvgScore float64 `json:"AvgScore,omitempty" gorm:"-"`
}

// QuizRate model
type QuizRate struct {
	gorm.Model

	RateScore int   `json:"RateScore"`
	QuizID    *uint `json:"QuizID"`
	Quiz      *Quiz `json:"Quiz"`
}

// Attachment model
type Attachment struct {
	gorm.Model

	Title  string  `json:"Title"`
	QuizID *uint   `json:"QuizID,omitempty" gorm:"default:null"`
	Quiz   *Quiz   `json:"Quiz,omitempty" gorm:"constraint:OnDelete:SET NULL;"`
	Url    *string `json:"Url,omitempty" gorm:"default:null"`
	Image  *string `json:"Image,omitempty" gorm:"default:null"`
	Score  int     `json:"Score" gorm:"default:0"`
}
